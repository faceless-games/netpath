# Chapter 4: Exercises

In the original tutorial, this chapter is dedicated to ensuring that the
tutorialee understands all the new concepts introduced (timers, scenes,
object orientation, etc).  However, this tutorial assumes you know most
of this already (as it comes with Javascript) or in the case of timers
and scenes, learned it in the previous chapter.  As such, there's not as
much in the way of exercises.

TODO: Come up with exercises other than "come up with exercises".