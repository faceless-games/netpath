# Chapter 1

## Boilerplate

As this is a web game, you'll need an html file to start:

**index.html**
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Netpath</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="navbar-brand">Netpath</div>
</nav>


<script type="module" src="./main.js"></script>
</body>
</html>
```

If you build this now, you'll likely get an error about `main.js` not being
found.  Let's fix that!

For our bare-bones purposes, there are three parts to this file.  First,
the configuration:

**main.js**
```typescript
import 'phaser';

let config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};
```

We'll be passing this into Phaser in a bit; for now it just tells it important
details such as how large the game area should be and what functions to call
for phaser lifecycle functions (more on that later).

The second part is to define those functions:

**main.js**, continued
```typescript
function preload () {
}

function create () {
}

function update () {
}
```

And the final part is to start up Phaser:

**main.js**, continued
```typescript
window.addEventListener('load', () => {
    let game = new Phaser.Game(config);
});
```

With that complete, you should be able to run your build system and web
server and see the grand result:  A big empty black box.

In the code above, once your project is run the functions above are called
in this order:

* **preload** is called as a setup step; this is typically where images would
  be loaded (as we'll demonstrate below).
* **create** is called when Phaser is ready to go; this is where you'll
  create all the objects for the game.
* **update** is called every frame.

A blank screen is how you know everything's working, but it's not very
entertaining.  Let's do something with those functions above:

**main.js**, revised
```typescript
let logo;

function preload() {
    this.load.setBaseURL('http://labs.phaser.io');
    this.load.image('logo', 'assets/sprites/phaser3-logo.png');
}

function create() {
    logo = this.add.image(400, 100, 'logo');
}
```

This demonstrates a few things about how Phaser handles things.  First, it
shows how `preload` works; you tell Phaser to load images here and you give
those images a **key** for later use.  Secondly, `create` creates a new image
from that key and places it on the screen.

Finally, let's make the logo a bit more likely, by changing its location every
frame:

**main.js**, revised
```typescript
function update () {
    logo.x = Phaser.Math.RND.integerInRange(0, 800);
    logo.y = Phaser.Math.RND.integerInRange(0, 600);
}
```

This will draw the logo in a different place every frame.

# Phaser Aside: The Game Loop

The original Bytepath tutorial goes into
[some detail](https://github.com/a327ex/blog/issues/15)
on the LÖVE game loop, which involves setting up the random state,
polling for events, calling the resulting functions, and so on.  This tutorial
omits all of this because while the phaser equivalent
[exists](https://phaser.io/phaser3/contributing/part7)
it's not intended to be altered by the game developer, unlike LÖVE.