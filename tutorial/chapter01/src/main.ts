

import 'phaser';

let config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

let logo;

function preload() {
    this.load.setBaseURL('http://labs.phaser.io');
    this.load.image('logo', 'assets/sprites/phaser3-logo.png');
}

function create() {
    logo = this.add.image(400, 100, 'logo');
}

function update () {
    logo.x = Phaser.Math.RND.integerInRange(0, 800);
    logo.y = Phaser.Math.RND.integerInRange(0, 600);
}

window.addEventListener('load', () => {
    let game = new Phaser.Game(config);
});