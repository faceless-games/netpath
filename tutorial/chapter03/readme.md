# Chapter 3: Scenes

Fitting an entire game into three functions and a bunch of global variables
would be less than ideal.  Fortunately, Phaser has the idea of 'scenes':
Separate states of the game that hold different groups of images and interact
differently from each other.  Imagine a JRPG: Walking around the overworld
would be one scene, fighting in combat another, the pause menu would be a
third, and so on.

If you've used other game engines, you might be familiar with them by
other names:
* Unity, Unreal, and Godot also use the "scene" terminology
* The bytepath tutorial, and Gamemaker that inspired it, call these "rooms"
* Clickteam calls these "frames"

In Lua/LÖVE, you'd have to create the entire scene framework from scratch,
but Phaser has them built in.

## Adapting our example to scenes

Here's how a phaser scene starts:

**ch3.ts**
```typescript
import 'phaser';

export class Chapter3Demo extends Phaser.Scene {
    
}
```

Note that we're breaking up the project into multiple files at this point;
while it's possible to develop a web game in one giant file (and indeed,
that's what our build system is creating) it's generally a lot more
maintainable to break the code up into modules.

You won't notice a change now, because our application isn't actually using
the class we just created.  For that, we'll have to change our config:

**main.js**, revised
```typescript
let config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    backgroundColor: '#000000',
    scene: [ Chapter3Demo ]
};
```

Run this and you'll see... A black square!  Let's add a bit of functionality:

**ch3.ts**
```typescript
import 'phaser';

export class Chapter3Demo extends Phaser.Scene {
    
    preload() {
        this.load.setBaseURL('http://labs.phaser.io');
        this.load.image('logo', 'assets/sprites/phaser3-logo.png');
    }

    create() {
        this.logo = this.add.image(400, 100, 'logo');
    }
    
    update () {
        this.logo.x = Phaser.Math.RND.integerInRange(0, 800);
        this.logo.y = Phaser.Math.RND.integerInRange(0, 600);
    }

}
```

Try this and you'll see the same thing we made back in Chapter 1: The Phaser
logo, blitting across the screen like crazy.  At this point, it doesn't look
like we've done anything differently, but we've actually unlocked an important
bit of functionality.  Namely, we can now have more than one scene!

Here's a simple second scene:

**ch3.ts**, appended
```typescript
export class StaticLogo extends Phaser.Scene {
    protected logo

    constructor ()
    {
        super('StaticLogo');
    }

    create() {
        this.logo = this.add.image(400, 200, 'logo');
    }
}
```

Note that we didn't have to use `preload` to load the textures for this scene;
once something's loaded, Phaser makes it available to every scene.

Another thing we did here was create a constructor for the scene which calls
up to its superclass with one argument.  That argument is the scene's *key*,
which we'll need later in order to transition to it.

Now that we have a second scene to switch to, we have to make Phaser aware
of it before we can actually switch.  This is a simple change to our config
in the main file:

**main.js**, revised
```typescript
let config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    backgroundColor: '#000000',
    scene: [ Chapter3Demo, StaticLogo ]
};
```

The only important thing about the order of scenes in the `scene` array is
that the first one will be the one that Phaser starts when the app is loaded.

Now we just tell our Chapter3Demo class to transition to this scene:

**ch3.ts**, revised
```typescript
export class Chapter3Demo extends Phaser.Scene {

    constructor() {
        super('Chapter3Demo');
    }

    // [...]

    create() {
        this.logo = this.add.image(400, 100, 'logo');
        this.time.addEvent({
            delay: 3000,
            callbackScope: this,
            callback: this.switchScenes
        })
    }

    switchScenes() {
        this.scene.start('StaticLogo');
    }
}
```

There's a lot going on here:

* First, we set a key for this scene as well.
* Secondly, we create a timed event.  The arguments for that event are:
  * The delay, in milliseconds, before that event happens,
  * The callback, a function that will be called after the delay, and
  * The callbackScope, which you pass `this` to to make sure that the `this`
    in the function works as expected.  How `this` works in Javascript is
    different from other languages and somewhat complex, but Phaser (and
    modern javascript) have a number of ways to mitigate this, as demonstrated.
* Finally, the callback actually switches scenes.  Unlike in the config,
  we call this with a string, specifically that scene's *key*.  A scene's key
  is, by default, 'default', but trying to have more than one scene with the
  same key will cause an error.

If you run this, you'll get the frantic phaser logo for three seconds, followed
by a scene where the logo sits still.

Even though this second scene is listed second in the list of scenes we
provided Phaser, that doesn't mean that the scenes can only transition
in one direction.  Phaser allows you to transition from any scene to any
other scene.

Here's how we'd adapt our new scene to transition back:

**ch3.ts**, revised
```typescript
export class StaticLogo extends Phaser.Scene {

  // [...]

  create() {
    this.logo = this.add.image(400, 200, 'logo');
    this.time.addEvent({
      delay: 3000,
      callback: () => {
        this.scene.start('Chapter3Demo')
      }
    })
  }
}
```

We used one of the solutions to the `this` problem mentioned before:
[an arrow function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions).
This is a feature of modern Javascript, but isn't supported by all
browsers.  For that matter, classes like we've been using aren't supported
by all browsers; Internet Explorer remains a holdout.  Build systems
can generally transpile these features, and using something like Typescript
will ensure you can use all of its features, but it's something to keep
in mind.

For the remainder of the tutorial, we'll be using modern JS features that
may not be supported by IE without note.  Anything else that's not supported
by a number of browsers will be noted.