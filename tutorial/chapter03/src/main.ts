
import 'phaser';
import {Chapter3Demo, StaticLogo} from "./ch3";

let config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    backgroundColor: '#000000',
    scene: [ Chapter3Demo, StaticLogo ]
};


window.addEventListener('load', () => {
    let game = new Phaser.Game(config);
});