import 'phaser';

export class Chapter3Demo extends Phaser.Scene {
    protected logo;

    constructor ()
    {
        super('Chapter3Demo');
    }

    preload() {
        this.load.setBaseURL('http://labs.phaser.io');
        this.load.image('logo', 'assets/sprites/phaser3-logo.png');
    }

    create() {
        this.logo = this.add.image(400, 100, 'logo');
        this.time.addEvent({
            delay: 3000,
            callbackScope: this,
            callback: this.switchScenes
        })
    }

    switchScenes() {
        this.scene.start('StaticLogo');
    }

    update () {
        this.logo.x = Phaser.Math.RND.integerInRange(0, 800);
        this.logo.y = Phaser.Math.RND.integerInRange(0, 600);
    }

}

export class StaticLogo extends Phaser.Scene {
    protected logo

    constructor ()
    {
        super('StaticLogo');
    }

    preload() {

    }

    create() {
        this.logo = this.add.image(400, 200, 'logo');
        this.time.addEvent({
            delay: 3000,
            callback: () => {
                this.scene.start('Chapter3Demo')
            }
        })
    }
}