# Chapter 2

TODO: Include any non-Phaser libraries I end up using here.

## Phaser Aside: Libraries

The [original tutorial](https://github.com/a327ex/blog/issues/16) has a
section and tutorials on additional non-LÖVE libraries that the overall
project uses.  However, Phaser either doesn't need them or has them built
in:

* Object-orientation comes to us courtesy of Typescript,
* Input in the style used in the tutorial is built into Phaser (e.g. `cursors`)
* Timers exist both in plain javascript and in Phaser. Though they are not
  as robust as the library timers (e.g. they can't chain), they're workable
  enough for this tutorial.