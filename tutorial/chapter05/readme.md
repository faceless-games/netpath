# Chapter 5: Game Basics

The previous chapters were dedicated to setting up Phaser and going through
a few of its features in an abstract way.  However, with this chapter, we're
going to start actually creating the whole game.

## Gameplay Structure

The gameplay should be identical to Bytepath, which means following the same
structure as it.  There will be three scenes:

* The **Stage** is the 'playing' scene and the one we'll be spending the most 
  time and effort on.  It's where all the gameplay happens and will have objects 
  such as the player, enemies, projectiles, etc.
* The **Console** is our 'main menu' scene.  Here is where the sound and 
  video settings, ship choices, etc will live.  It's also where we'll 
  transition into the Stage and SkillTree scenes.
* Finally, the **SkillTree** is exactly what it sounds like: A scene where 
  we'll display an entire passive skill tree and allow the player to select 
  from items on it.  Like Bytepath, we'll be able to support a
  [massive skill tree](https://steamcommunity.com/sharedfiles/filedetails/?id=1312004070)
  though creating it in its entirety is left as an exercise for the reader.

## Game Size

The original Bytepath's display is 1440x810, but internally its resolution 
is 480x270 and it just scales it up by 3 to achieve a pixelated look.  We'll 
be doing the same thing but in a somewhat different way.  Whereas the original
tutorial draws the game onto a secondary canvas and scales it up, we'll be
using the built-in Phaser scaling manager.

In order to see the difference, let's first start with creating our 
'Stage' scene and drawing a circle:

**stage.js**
```typescript
import 'phaser'

export class Stage extends Phaser.Scene {

    create() {
        this.graphics = this.add.graphics({x: 0, y: 0});
        this.graphics.lineStyle(1, 0xffffff, 1);
        this.graphics.strokeCircle(100, 100, 50);
    }
}
```

In order to see it, we'll need to update the phaser config in main.js:

**main.js**
```typescript
import 'phaser';
import {Stage} from "./stage";

let config = {
    type: Phaser.AUTO,
    width: 480,
    height: 270,
    backgroundColor: '#000000',
    scene: [ Stage ]
};


window.addEventListener('load', () => {
    let game = new Phaser.Game(config);
});
```

With this you should see a 480x270 screen with a smoothly drawn circle in 
the middle.  Now, we'll scale the screen up with this new config:

**main.js**, revised
```typescript
let config = {
    type: Phaser.AUTO,
    width: 480,
    height: 270,
    scale: {
        mode: Phaser.Scale.NONE,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        zoom: 3,
    },
    backgroundColor: '#000000',
    scene: [ Stage ]
};
```

This is doing a few things.  First, it's using Phaser's
[scale manager](https://photonstorm.github.io/phaser3-docs/Phaser.Scale.ScaleManager.html)
to set up some things about our game.  To get that pixel-look, we're applying
a `zoom` factor of 3.  We're also using the `mode` to tell Phaser not to do
any additional scaling beyond that.  Finally, `autoCenter` is some nice
utility that'll ensure that the game is always centered in its parent.

There's one problem, though:  Depending on your browser and settings, it's
entirely possible that the game window doesn't fit in the browser!  Moreover,
even if it does, it's entirely possible to resize the browser so that this is
no longer the case.

Fortunately, you can account for this.  Change the `mode` to
`Phaser.Scale.FIT` and resize the browser; you should instantly see the change.
This setting ensures that the window is scaled to fit while trying to respect
the aspect ratio set by the original width and height.

## Camera

Phaser has a built-in camera system for its scenes; by default, there's one
camera that has a viewport the size of the game and that's the camera that's
in use.  While you can do interesting camera-in-camera tricks, it won't be
necessary for our purposes.  Later in the tutorial we'll cover using it to do
useful things like scrolling (given the size of the passive tree, that's an 
outright requirement, after all) but for now I'll demonstrate a simple trick
we'll make much use of:  Screen shake!

**stage.js**
```typescript
import 'phaser'

export class Stage extends Phaser.Scene {

    create() {
        this.graphics = this.add.graphics({x: 0, y: 0});
        this.graphics.lineStyle(1, 0xffffff, 1);
        this.graphics.strokeCircle(100, 100, 50);

        this.cameras.main.shake(5000, 0.005);
    }
}
```

If you're like me, you're quickly  switching back and forth between your IDE 
and the browser in the hopes of seeing the screen shake effect.  To make our
lives easier, let's just make it a button you can press:

**stage.js**
```typescript
import 'phaser'

export class Stage extends Phaser.Scene {

    create() {
        this.graphics = this.add.graphics({x: 0, y: 0});
        this.graphics.lineStyle(1, 0xffffff, 1);
        this.graphics.strokeCircle(100, 100, 50);

        let f3 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.F3);
        f3.on('down', () => {
          this.cameras.main.shake(500, 0.005);
        })
    }
}
```

Now, pressing F3 shakes the screen for half a second.

### Phaser aside: Input

The original tutorial uses the kind of event-driven input I've demonstrated 
above, but Phaser provides quite a few ways to check if a key's pressed or not.
For UI actions or other things where you'll hold down the button, it's often 
more useful to simply check if a button is currently held down rather than 
set a variable via event.  

## Player Object

We now have everything we need to create the actual player object:

**player.js**
```typescript
import 'phaser'

export class Player extends Phaser.GameObjects.Graphics {
    constructor(scene, options) {
        super(scene, options);
    }

    addedToScene() {
        super.addedToScene();
        this.lineStyle(1, 0xFFFFFF, 1.0);
        this.fillStyle(0xFFFFFF, 1.0);
        this.strokeCircle(0, 0, 25);
    }
}
```

Every possible kind of object that appears on the screen is one of Phaser's 
`GameObject` classes.  If you're creating your own custom gameobject, it's 
very useful to find the specfic kind of GameObject your custom item is most 
like, and extend that.  So if you have e.g. a moving sprite that represents 
your player, you'd likely want to inherit from `Phaser.GameObjects.Sprite`.  
In this case, `Phaser.GameObjects.Graphics` is the class that allows us to 
draw graphics primitives like lines, circles, etc, to the screen.  So far, 
we haven't done anything that couldn't be done with a non-custom Graphics 
object, but that'll change as we expand.

For now, let's just get it on the screen:

**stage.js**
```typescript
import 'phaser'
import {Player} from "./player";

export class Stage extends Phaser.Scene {

  create() {
    this.player = new Player(this, {
      x: this.cameras.main.centerX,
      y: this.cameras.main.centerY
    });
    this.add.existing(this.player);

    let f3 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.F3);
    f3.on('down', () => {
      this.player.destroy();
    });
  }
}
```

This does a few things:
* First, we've removed the old `Graphics` that we were using to draw a circle.
* Secondly, we're creating an instance of the Player class we just wrote.
* Thirdly, we're adding it to the scene via `this.add.existing`.
* Finally, we wire up the `f3` key to kill the player, demonstrating how to 
  remove objects that we've added.

### Phaser Aside: Custom gameobjects

Most adding of items to phaser are of the form e.g.  `this.add.sprite` to 
add sprites or `this.add.graphics` to add our original Graphics object.  
We have to use `this.add.existing` here because of course Phaser wouldn't 
know how to handle something like `this.add.player`.  But what if it *could*?

As it turns out, this is something that Phaser actually supports!  The first 
half, creating a custom subclass, we've already done.  The second half is 
using a phaser plugin to register that subclass in Phaser itself, so that 
it'll then recognize e.g. `this.add.player`.  While doing this is outside 
the scope of the tutorial, phaser has a number of
[examples](https://phaser.io/examples/v3/view/plugins/custom-game-object)
of how to do so.

## Player Physics

The physics of the player (and other objects in the game) are rather simple. 
While we could use a full-fledged physics simulation via Phaser's built-in
"Matter" engine, that's massive overkill for what we're going to be doing.  
Instead, we'll use its aptly name "arcade" physics.

The first step is enabling it in the config:

**main.js**, revised
```typescript
let config = {
    type: Phaser.AUTO,
    width: 480,
    height: 270,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        zoom: 3,
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 0},
            debug: true
        }
    },
    backgroundColor: '#000000',
    scene: [ Stage ]
};
```

The 'physics' portion sets our default engine as 'arcade' and sets up some 
configuration for it, namely turning gravity off and enabling the debug 
settings.  That latter will come in handy in just a moment!

Just enabling physics doesn't do anything; adding something to the game 
world doesn't actually add it to the *physics* world.  It's somewhat 
confusing, so it's helpful to keep in mind that as far as Phaser is 
concerned, physics and graphics are entirely separate systems.  In some 
cases they'll be synced up for us (namely positioning on-screen in response 
to other forces operating on the object) but in some cases they won't be, 
and this is one of them.  Thus:

**stage.js**
```typescript
    [...]
        this.player = new Player(this, {
            x: this.cameras.main.centerX,
            y: this.cameras.main.centerY
        });
        this.physics.add.existing(this.player);
        this.add.existing(this.player);
    [...]

```

That order is important, in that we'll be setting up some physics items when 
the player is added to the screen, so we have to add it to physics first so 
they exist when we do that.

Run this and it will work, but you'll immediately notice something odd. The 
white circle representing our player appears as expected, but there's 
also a magenta square overlapping it.  This is the debug information from the 
physics engine that we configured earlier, and it's showing us something 
important:  As far as the physics engine is concerned, that purple square is 
what our Player looks like.  If we played the game like this, we'd be 
crashing into things that visibly didn't intersect the circle, while some 
things that did would be missed.

Fortunately, while the physics system does its best to guess what added items
are shaped like, we can override that guess with accurate information.  In 
this case, I'm going to include the information in the object itself:

**player.js**
```typescript
import 'phaser'

export class Player extends Phaser.GameObjects.Graphics {
  constructor(scene, options) {
    super(scene, options);
  }

  addedToScene() {
    super.addedToScene();
    this.lineStyle(1, 0xFFFFFF, 1.0);
    this.fillStyle(0xFFFFFF, 1.0);
    this.strokeCircle(0, 0, 25);
    this.body.setCircle(25, -25, -25);
  }
}
```

The reference to `this.body` is why we made sure to add the Player object to 
the physics simulation before adding it to the screen.  `addedToScene` 
is called when the object is added to the Phaser scene, and if we did that 
before adding it to the physics system, then `this.body` wouldn't exist.

By default, the physics body system assumes that the top left of the body is 
{x:0, y:0}, but we've been using that as the center; as such, we pass in -25 
as X and Y offsets to make sure the body ends up in the right place.

Run this and you'll see the magenta square has vanished, and only a magenta 
circle remains.  (Since it's perfectly overlapping the regular circle we're 
drawing, the latter isn't visible)

Now that we've got the physics object in sync with the on-screen object, we 
can actually use the physics system we've gone through all this trouble to 
set up!

First, let's add some physics-related variables to our player:

**player.js**, revised
```javascript
import 'phaser'

export class Player extends Phaser.GameObjects.Graphics {
    constructor(scene, options) {
        super(scene, options);
        this.rotation = 0;
        this.rotationPerSecond = 1.66*Math.PI;
        this.speed = 0;
        this.maxSpeed = 100;
        this.accel = 100;
        // [...]
```

These variables are:
* **rotation**, the current rotation of the player, in radians.  Zero is facing 
  the right of the screen, and increasing this number rotates clockwise.
* **rotationPerSecond** is how many radians the player can rotate in a second.
* **speed** is the current speed of the player, in pixels per second
* **maxSpeed** is the maximum speed we'll allow
* **accel** is how much we'll be accelerating, i.e. how much we'll be adding 
  to *speed** per second.

While we're add it, let's add a few keys so we can control the player.  
Immediately after the previous code, add:

```javascript
        this.left = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.right = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
    }
```

Those keycodes represent the arrow keys; if you'd rather steer with e.g. 
WASD, feel free to change these lines accordingly!

Now, let's actually move.  For this, we need to add an `update` function to 
the Player class:

```javascript
    update(ts, delta) {
        super.update(ts, delta);
        let dt = delta / 1000;

        if(this.left.isDown) this.rotation -= this.rotationPerSecond * dt;
        if(this.right.isDown) this.rotation += this.rotationPerSecond * dt;

        this.speed = Math.min(this.maxSpeed, this.speed + this.accel * dt);
        this.scene.physics.velocityFromRotation(
            this.rotation,
            this.speed,
            this.body.velocity,
        );
    }
```

This function:
* Takes two arguments: a timestamp and the elapsed time since the last frame.
  That elapsed time is in milliseconds and all our other variables are "per 
  second" so we immediately convert from ms to seconds.
* Checks to see if the left and right keys we set up earlier are currently 
  being pressed
  * If so, we change the rotation by our rotationPerSecond.  We multiply by 
    dt because it's unlikely exactly one second has passed since the last 
    frame, so we need to scale the number by how much time *has* passed.  If 
    we didn't do this, the rotation speed of the ship would depend on how 
    fast the game was running in the user's browser.
* Applies the acceleration (again scaled by time) to the current speed, and 
  then sets the current speed to the minimum of that or our speed cap.
* Finally, we use Phaser's `velocityFromRotation` function to calculate what 
  the velocity of the ship should be, given its rotation and speed.  The last 
  argument to `velocityFromRotation` is optional: the function will actually 
  *set* the given vector to the result (rather than returning a new vector). 
  We pass in `this.body.velocity` so that the appropriate velocity is set 
  directly on the physics body of the player.

Run this and you'll see... nothing happen!?

As it turns out, Phaser doesn't actually call `update` for custom components 
by default.  This is one of those things that Phaser is opinionated about; 
it expects us to give full control of sprites and such to the physics system 
and only interact at a high level.

Fortunately, there are two options:

* First, and easiest, is that we can call the player's `update` function 
  from the scene's `update` function.  This works, but it means that in the 
  future, we'd have to keep a list of *all* our custom objects and update 
  them individually.
* Secondly, we can create a Phaser group.  Groups are exactly what they 
  sound like: groups of gameobjects.  Groups have a lot of customization 
  options available to them, one of which is called `runChildUpdate`.  This 
  means that anything we add to the group will have its update function 
  called every frame.

I've gone with the second approach, as I feel the first would just be 
replicating what Phaser does on its own.

**stage.js**
```javascript
import 'phaser'
import {Player} from "./player";

export class Stage extends Phaser.Scene {

    create() {
        this.player = new Player(this, {
            x: this.cameras.main.centerX,
            y: this.cameras.main.centerY
        });
        this.physics.add.existing(this.player);
        this.add.existing(this.player);

        this.customGroup = this.add.group({
            runChildUpdate: true,
        })
        this.customGroup.add(this.player);

        let f3 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.F3);
        f3.on('down', () => {
            this.player.destroy();
        });
    }
}
```

Now our stage has a group for every custom object we've made.  Currently 
that's just the player, but we'll likely be adding other objects in the future.

Run this and you should now see your circle accelerate!  Use the left and 
right arrow keys (or whatever keys you set up, if you changed it) to steer 
left or right.

If you still have debug mode on for physics, this looks fine because one of 
the features of debug mode is that it draws the current velocity of objects. 
If you turn it off, however, you'll immediately notice a problem: there's no 
indication as to which way the player's actually facing.

Let's fix that:

**player.js**
```javascript
// [...]
addedToScene() {
  super.addedToScene();
  this.lineStyle(1, 0xFFFFFF, 1.0);
  this.fillStyle(0xFFFFFF, 1.0);
  this.strokeCircle(0, 0, 10);
  this.lineBetween(0, 0, 20, 0);

  this.body.setCircle(10, -10, -10);
}
// [...]
```

The current player object was a bit big, so I shrank it down a bit.  The 
`lineBetween` function draws a line between the center of the player to a 
spot 20 pixels to the right, so we can see what direction the player is moving.

You might notice that we don't have to change these numbers to account for 
the player's rotation.  The answer to why is that we specifically used the 
variable `rotation`:  Every Phaser gameobject has a `rotation` variable that 
affects what angle they're drawn at.  By using that same variable, we're 
telling Phaser to rotate the player automatically.  

For an experiment, you can change the name of the 'rotation' variable to 
something else (e.g. 'schmotation') and you'll see that while the physics 
system still uses the correct rotation, the graphics themselves are no 
longer rotated for us.  Just be sure to change it back before moving on!

### Phaser aside: Physics

When making a game in Phaser, you've got a spectrum of options when it comes to 
physics:
* Use Phaser's built-in Arcade physics (or Matter, for more involved 
  simulations) entirely.
* Roll it yourself; this is most feasible for very simple physics 
  like e.g. platformers.

We've chosen a middle ground for the reason that it's roughly analogous to 
how the original tutorial does things:  it calculates some physics on its 
own (e.g. rotation as it applies to acceleration) but relies on the 
underlying simulation library to do the rest (e.g. moving the player 
according to its current velocity).

The full 'Phaser way' to do this would be to set angular acceleration when 
turning, and apply regular acceleration from there, as demonstrated in
[this Phaser example](https://phaser.io/examples/v3/view/physics/arcade/asteroids-movement).

## Garbage collection (TODO)

The lua tutorial has quite a bit to say on the topic of garbage 
collection, some of which is still applicable:

* Nilling out references so that they don't stick around is not applicable 
  *yet* but probably will be once projectiles and such enter the mix.
* We don't need a custom `destroy` since things like physics bodies are 
  already destroyed when we call the regular gameobject destroy (verified by 
  turning debug mode on and using F3 to kill the player)
  * TODO: Make sure things like the keybinds we made in the constructor are 
    also cleaned up.  Next chapter has a timer that we'll likely need to stop.
* There's a fair bit of ad-hoc profiling; it might be useful to go over 
  browser profiling tools.
